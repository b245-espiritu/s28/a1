
// inserting one room
db.rooms.insertOne(
    {
        name: 'single',
        accomodates: 2,
        price: 1000,
        description: 'A simple room with all the basic necessities',
        roomAvailable: 10,
        isAvailable: false

    }
);


//inserting many rooms
db.rooms.insertMany([
    {
        name: 'double',
        accomodates: 3,
        price: 2000,
        description: 'A room fit foa a small family going on a vacation',
        roomAvailable: 5 ,
        isAvailable: false
    },

    {
        name: 'queen',
        accomodates: 4,
        price: 4000,
        description: 'A room with a queen sized bed perfect for a simple getaway',
        roomAvailable: 15,
        isAvailable: false
    }
]);


// finding room with name 'double'
db.rooms.find({name: 'double'});


//updating one room
db.rooms.updateOne({name:'queen'}, {$set: {roomAvailable: 0}});


//deleting rooms with 0 available room or roomAvailable: 0
db.rooms.deleteMany({roomAvailable:0});